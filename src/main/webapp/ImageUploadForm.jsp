<%--
  Created by IntelliJ IDEA.
  User: jimbocorn
  Date: 12-01-16
  Time: 12:29 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Image Service</title>
</head>
<body>
    <h1>Image Service</h1>

    <form method="POST" enctype="multipart/form-data"
          action="/imageUploadForm">
        File to upload: <input type="file" name="file"><br />
        <input type="submit" value="Upload">
    </form>
</body>
</html>
