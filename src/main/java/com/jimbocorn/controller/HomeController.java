package com.jimbocorn.controller;

import com.jimbocorn.model.ImageUpload;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by jimbocorn on 09-01-16.
 */
@Controller
public class HomeController{

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public ModelAndView getImageForm(){

        ModelAndView model = new ModelAndView("ImageUploadForm");
        return model;
    }

    @RequestMapping(value = "/imageUploadForm",method = RequestMethod.POST)
    public ModelAndView processImage(@RequestParam("file") MultipartFile file){

        ModelAndView model = new ModelAndView("Summary");
        //TODO : use dependency injection
        ImageUpload imageFile = new ImageUpload();
        imageFile.setImageUpload(file);

        if (!file.isEmpty()) {
            if (isImage(file)){
                model.addObject("permanentLink","This is the link to your Image: " + generatePermanentlink(file.getOriginalFilename()));
                return model;
            }else{
                model.addObject("msg","File is not an image");
                return model;
            }
        } else {
            model.addObject("msg","Failed to upload because the file was empty.");
            return model;
        }
    }

    public boolean isImage(MultipartFile file){

        String mimetype= file.getContentType();
        String type = mimetype.split("/")[0];
        if (type.equals("image")) {
            //errors.rejectValue("file","required.imageType");
            return true;
        }else return false;
    }

//    private ImageUpload transformInBytes(MultipartFile file){
//
//        try {
//            byte[] bytes = file.getBytes();
//            BufferedOutputStream stream =
//                    new BufferedOutputStream(new FileOutputStream(new File("userImage")));
//            stream.write(bytes);
//            stream.close();
//            return ;
//        } catch (Exception e) {
//            model.addObject("msg","Failed to upload " + name + " => " + e.getMessage());
//            return model;
//        }
//    }
    private String generatePermanentlink(String fileName){
        //TODO elaborate more on the generated link
        String baseURL = "/service/";
        return baseURL+fileName;
    }
}
