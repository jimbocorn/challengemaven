package com.jimbocorn.model;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by jimbocorn on 12-01-16.
 */
public class ImageUpload {
    MultipartFile file;

    public MultipartFile getImageUpload() {
        return file;
    }

    public void setImageUpload(MultipartFile file) {
        this.file = file;
    }

}
